﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public int enemyQuantity = 50;
    public void Initialize()
    {
        GameController.OnStepMade.AddListener(SpawnEnemies);
    }

    private void SpawnEnemies()
    {
        Debug.Log("EnemiesSpawned");
        for (int i = 0; i < enemyQuantity;i++)
        {
            SpawnCreature(new Vector3Int(UnityEngine.Random.Range(-WorldGrid.MapSize / 2, +WorldGrid.MapSize / 2), UnityEngine.Random.Range(-WorldGrid.MapSize / 2, +WorldGrid.MapSize / 2), 0));
        }
    }

    private void SpawnCreature(Vector3Int pos)
    {
        //Спавн в рандомном с землей 3x3
        int n = 1;
        Vector3Int tilePos = pos;
        while (true)
        {
            if (CheckNTilesWithStep(ref tilePos, ref n, Vector3Int.up, (position) => WorldGrid.Instance.CanSpawnHere(position)))
                break;
            if (CheckNTilesWithStep(ref tilePos, ref n, Vector3Int.right, (position) => WorldGrid.Instance.CanSpawnHere(position)))
                break;
            n++;
            if (CheckNTilesWithStep(ref tilePos, ref n, Vector3Int.down, (position) => WorldGrid.Instance.CanSpawnHere(position)))
                break;
            if (CheckNTilesWithStep(ref tilePos, ref n, Vector3Int.left, (position) => WorldGrid.Instance.CanSpawnHere(position)))
                break;
            n++;
        }

        Creature newCreature = Instantiate(enemyPrefab).GetComponent<Creature>();
        newCreature.creaturePosition = tilePos;
        WorldGrid.tilemaps[WorldGrid.CREATURES_TILEMAP_NAME].SetTile(tilePos, newCreature.creatureTile);
    }
    bool CheckNTilesWithStep(ref Vector3Int position, ref int n, Vector3Int step, Predicate<Vector3Int> IsValid)
    {
        Vector3Int origin = position;
        do
        {
            if (IsValid(position)) return true;
            position += step;
        }
        while (position != origin + step * n);

        return false;
    }
}
