﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;

public class MouseDrawing : MonoBehaviour
{
    public GameObject enemyPrefab;
    public Tilemap drawTilemap;
    public MouseDrawingSettings[] drawingSettings;
    public Dictionary<string, MouseDrawingSettings> drawingSettingsD;
    private MouseDrawingSettings currentDrawingSettings;
    // Start is called before the first frame update
    delegate void DrawingMethod(Vector3Int position);
    DrawingMethod metaDrawing;
    public void Initialize()
    {
        drawingSettingsD = new Dictionary<string, MouseDrawingSettings>();
        foreach (var settings in drawingSettings)
        {
            drawingSettingsD.Add(settings.tilemapName, settings);
        }
        currentDrawingSettings = drawingSettingsD[WorldGrid.GROUND_TILEMAP_NAME];
        drawTilemap = WorldGrid.tilemaps[WorldGrid.GROUND_TILEMAP_NAME];
        drawTilemap.SetTile(Vector3Int.zero, drawingSettingsD[WorldGrid.GROUND_TILEMAP_NAME].drawTile);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3Int cellPos = drawTilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        if (Input.GetMouseButton(0))
        {
            Draw1(cellPos, metaDrawing);
        }
        else if(Input.GetMouseButton(1))
        {
            Draw5(cellPos, metaDrawing);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            drawTilemap = WorldGrid.tilemaps[WorldGrid.GROUND_TILEMAP_NAME];
            currentDrawingSettings = drawingSettingsD[WorldGrid.GROUND_TILEMAP_NAME];
            metaDrawing = null;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            drawTilemap = WorldGrid.tilemaps[WorldGrid.BERRIES_TILEMAP_NAME];
            currentDrawingSettings = drawingSettingsD[WorldGrid.BERRIES_TILEMAP_NAME];
            metaDrawing = null;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            drawTilemap = WorldGrid.tilemaps[WorldGrid.CREATURES_TILEMAP_NAME];
            currentDrawingSettings = drawingSettingsD[WorldGrid.CREATURES_TILEMAP_NAME];

            metaDrawing = (position) =>
            {
                Creature newCreature = Instantiate(enemyPrefab).GetComponent<Creature>();
                newCreature.creaturePosition = position;
            };
        }

        if (Input.GetMouseButtonDown(2))
        {
            if (WorldGrid.Instance.CanSpawnHere(cellPos))
            {
                Debug.Log(cellPos + "  Можно спавниться ");
            }
            else
            {
                Debug.Log(cellPos + "  Нельzя спавниться");
            }
        }
    }

    void Draw1(Vector3Int position, DrawingMethod method)
    {
        if (position != Vector3Int.zero && drawTilemap.GetTile(position) != currentDrawingSettings.drawTile)
        {
            drawTilemap.SetTile(position, currentDrawingSettings.drawTile);
            method?.Invoke(position);
        }
    }
    void Draw5(Vector3Int position, DrawingMethod method)
    {
        Draw1(position, method);
        Draw1(position + Vector3Int.down, method);
        Draw1(position + Vector3Int.up, method);
        Draw1(position + Vector3Int.left, method);
        Draw1(position + Vector3Int.right, method);
    }
}
