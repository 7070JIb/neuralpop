﻿using System.Collections;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Events;
using Assets.Scripts;
using System;
using Neural_Network_1;

public class GameController : MonoBehaviour
{
    public static Player player;


    public static UnityEvent OnStepMade;
    public static UnityEvent OnGameStarted;

    public static UnityEvent1<Creature> OnCreatureSpawned;
    public static UnityEvent1<Player> AfterPlayerSpawned;

    public GameObject playerPrefab;
    public float stepTime;
    [Space]
    public int Endurance;
    public int Health;
    public int MaxAir;
    public int Air;
    public int Strength;
    public int Protection;
    public int Reaction;
    [Space]
    bool IsGameStarted = false;
    private float timer;

    public static NeuralNetwork playerNN;
    public void Initialize()
    {
        
        OnGameStarted = new UnityEvent();
        OnStepMade = new UnityEvent();
        OnCreatureSpawned = new UnityEvent1<Creature>();
        AfterPlayerSpawned = new UnityEvent1<Player>();
        OnStepMade.AddListener(MakeStepsForEveryone);
    }

    public void StartGame()
    {
        IsGameStarted = true;
        timer = 0;

        SpawnPlayer();
        player.IsUIConnected = true;


        OnGameStarted.Invoke();
    }

    void Update()
    {
        if (IsGameStarted)
        {
            if (timer >= stepTime)
            {
                timer = 0;
                OnStepMade.Invoke();
            }
            timer += Time.deltaTime;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartGame();
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartGame();
            }
        } 
    }

    void MakeStepsForEveryone()
    {
        List<Creature> newListOfCreatures = new List<Creature>();
        foreach (var creature in WorldGrid.Instance.allCreatures)
        {
            if (!creature.IsDead)
            {
                newListOfCreatures.Add(creature);
                creature.TryToMakeAction();
            }
        }
        WorldGrid.Instance.allCreatures = newListOfCreatures;
        Debug.Log("Steps made for everyone");
    }
    Player SpawnPlayer()
    {
        GameObject playerGO = Instantiate(playerPrefab);
        player = playerGO.GetComponent<Player>();
        player.SetParameters(Endurance, MaxAir, Strength, Protection, Reaction, Health, MaxAir);

        Camera.main.GetComponent<MainCamera>().player = player;
        Camera.main.orthographicSize = 10;
        playerNN = new NeuralNetwork(new int[] { MoveData.MOVEDATA_NUM, MoveData.MOVEDATA_NUM, MoveData.MOVEDATA_NUM, 5 });

        OnCreatureSpawned.Invoke(player);
        AfterPlayerSpawned.Invoke(player);
        player.OnDied.AddListener(CreatePlayerDataset);
        return player;
    }

    private void CreatePlayerDataset()
    {
        var dataset = NeuralNetwork.ParseTVSAsync(@"C:\Users\romoc\NeuroPopulation\MoveData\player.tsv");
    }
}
