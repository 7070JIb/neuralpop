﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Assets.Scripts;
using System.IO;
using Neural_Network_1;
using UnityEngine.Events;
using System.Linq;

public class Player : Creature
{
    public Tile playerTile;
    public Tile[] arrowTiles;
    private Tilemap arrows;
    public List<MoveData> moveDataset = new List<MoveData>();

    public UnityEvent1<float> OnSuggestionMade;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            SetMoveDirection(Vector3Int.up);
        }
        else
        if (Input.GetKeyDown(KeyCode.D))
        {
            SetMoveDirection(Vector3Int.right);
        }
        else
        if (Input.GetKeyDown(KeyCode.S))
        {
            SetMoveDirection(Vector3Int.down);
        }
        else
        if (Input.GetKeyDown(KeyCode.A))
        {
            SetMoveDirection(Vector3Int.left);
        }
    }
    void SetMoveDirection(Vector3Int direction)
    {
        //Если то же направление задается второй раз, то существо будет стоять 
        if(direction != moveDirection)
        {
            moveDirection = direction;
        }
        else
        {
            moveDirection = Vector3Int.zero;
        }

        //Удалить стрелку, если задается направление - поставить новую
        SetArrows();
    }
    void SetArrows()
    {
        arrows.ClearAllTiles();
        if (moveDirection != Vector3Int.zero)
        {
            arrows.SetTile(creaturePosition + moveDirection, GetArrowTile(moveDirection));
        }
    }
    private Tile GetArrowTile(Vector3Int direction)
    {
        if (direction == Vector3Int.up)
        {
            return arrowTiles[0];
        }
        else if (direction == Vector3Int.right)
        {
            return arrowTiles[1];
        }
        else if (direction == Vector3Int.down)
        {
            return arrowTiles[2];
        }
        else if (direction == Vector3Int.left)
        {
            return arrowTiles[3];
        }
        else Debug.LogError("Конченное направление стрелки");

        return emptyTile;
    }
    public override void AtAwake()
    {
        OnSuggestionMade = new UnityEvent1<float>();
        WorldGrid.Instance.allCreatures.Insert(0, this);
        this.OnDied.AddListener(WriteDatasetInFile);
        this.OnDied.AddListener(Reproduce);
        GameController.OnStepMade.AddListener(MakeSuggestion);
    }

    private void MakeSuggestion()
    {
        float [] situation = CreateSituationData();
        Debug.Log($"Situation contains {situation.Length} elements");
        double[] result = GameController.playerNN.FeedForward(situation.Select((t)=>(double)t).ToArray());
        float maxi = 0;
        Debug.Log("results: ");

        for (int i = 0; i < result.Length;i++)
        {
            Debug.Log(""+i+": " + String.Format("{0: 0.000}",result[i]));
            if (result[i] == result.Max()) maxi = i;
        }
        OnSuggestionMade.Invoke(maxi);

    }

    public override void AtStart()
    {

        arrows = WorldGrid.tilemaps[WorldGrid.ARROWS_TILEMAP_NEM];

        //Спавн в рандомном с землей 3x3
        int n = 1;
        Vector3Int tilePos = new Vector3Int(UnityEngine.Random.Range(-WorldGrid.MapSize/2, + WorldGrid.MapSize/2), UnityEngine.Random.Range(-WorldGrid.MapSize / 2, +WorldGrid.MapSize / 2),0);
        while (true)
        {
            if (CheckNTilesWithStep(ref tilePos, ref n, Vector3Int.up, (position) => WorldGrid.Instance.CanSpawnHere(position))) 
                break;
            if (CheckNTilesWithStep(ref tilePos, ref n, Vector3Int.right, (position) => WorldGrid.Instance.CanSpawnHere(position)))
                break;
            n++;
            if (CheckNTilesWithStep(ref tilePos, ref n, Vector3Int.down, (position) => WorldGrid.Instance.CanSpawnHere(position)))
                break;
            if (CheckNTilesWithStep(ref tilePos, ref n, Vector3Int.left, (position) => WorldGrid.Instance.CanSpawnHere(position)))
                break;
            n++;
        }

        creaturePosition = tilePos;

        creaturesTilemap.SetTile(tilePos, playerTile);
        creatureTile = playerTile;
       
    }
    bool CheckNTilesWithStep(ref Vector3Int position, ref int n, Vector3Int step, Predicate<Vector3Int> IsValid)
    {
        Vector3Int origin = position;
        do
        {
            if (IsValid(position)) return true;
            position += step;
        }
        while (position != origin + step*n);

        return false;
    }
    public override void TryToMakeAction()
    {
        //Создается инфа о ТЕКУЩЕМ ходе(перед совершением, но с известным направлением)
        CreateMoveData();
        if (WorldGrid.GetCreature(creaturePosition + moveDirection, out Creature opponent) && opponent != this)
        {
                Attack(opponent);
        }
        else
        {
            MakeStep(moveDirection);
            SetArrows();
        }

        lifeInStepsCount++;
    }

    public override MoveData CreateMoveData()
    {
        TileInfo[] tilesData = new TileInfo[(GameSettings.creatureViewDistance * 2 + 1) * (GameSettings.creatureViewDistance * 2 + 1)];
        //Нулевая ячейка для инфы о тайле игрока (земля или нет)
        tilesData[0] = new TileInfo(
            new List<float>
            {
                ((WorldGrid.groundTiles[creaturePosition]) ? 1 : 0)
            },
            new List<string>
            {
                "суша под игроком"
            });
        int i = 1;
        for (int x = creaturePosition.x - GameSettings.creatureViewDistance; x <= creaturePosition.x + GameSettings.creatureViewDistance; x++)
        {
            for (int y = creaturePosition.y - GameSettings.creatureViewDistance; y <= creaturePosition.y + GameSettings.creatureViewDistance; y++, i++)
            {
                if (creaturePosition != new Vector3Int(x, y, 0))
                    tilesData[i] = TileInfo.ReadFromTile(new Vector3Int(x, y, 0), this);
                else
                    i--;
            }
        }

        MoveData moveData = new MoveData(this, tilesData);
        moveDataset.Add(moveData);
        return moveData;
    }

    public void WriteDatasetInFile()
    {
        if (File.Exists("MoveData/player.tsv")) File.Delete("MoveData/player.tsv");
        StreamWriter writer = new StreamWriter(new FileStream("MoveData/player.tsv", FileMode.Create));
        foreach(var text in MoveData.dataText)
        {
            writer.Write(text + "\t");
        }
        writer.Write(writer.NewLine);
        foreach (var moveData in moveDataset)
        {
            for(int i = 0; i < moveData.data.Count; i++)
            {
                writer.Write(moveData.data[i]);
                if (i != moveData.data.Count - 1)
                {
                    writer.Write("\t");
                }
            }
            writer.Write(writer.NewLine);
        }
        writer.Flush();
        writer.Close();
    }

    protected override void Reproduce()
    {
        GameController.playerNN.Learn(NeuralNetwork.ParseTVS(@"C:\Users\romoc\NeuroPopulation\MoveData\player.tsv"),10,0.7f);
    }

    public override float[] CreateSituationData()
    {
       MoveData moveData =  CreateMoveData();
        return moveData.data.Skip(5).ToArray();
    }
}
