﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;
using System;
using UnityEngine.Events;
using Assets.Scripts.UI;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public GameObject creatureInfoPrefab;
    
    public GameObject playerUI;
    public GameObject nnSuggestion;
    public Sprite[] arrows;
    private Player player;

    public void Initialize()
    {
        GameController.OnCreatureSpawned.AddListener(SpawnCreatureInfoUI);
        GameController.AfterPlayerSpawned.AddListener(CreatePlayerInfoUI);
    }

    private void CreatePlayerInfoUI(Player player)
    {       
        this.player = player;
        foreach (var parameter in playerUI.transform.GetComponentsInChildren<CreatureParameterUI>())
        {
            player.OnParameterChanged_ByType[parameter.Type].AddListener(parameter.OnParameterUpdated);
            parameter.Initialize(player);
        }
        //Эти циклы должны быть раздельными
        foreach (var parameter in playerUI.transform.GetComponentsInChildren<CreatureParameterUI>())
        {
            player.OnParameterChanged_ByType[parameter.Type].Invoke(player.parametersByType[parameter.Type]);
        }

        nnSuggestion.GetComponent<NNSuggestionUI>().Initialize(player);
    }

    private void SpawnCreatureInfoUI(Creature creature)
    {
        GameObject newCreatureInfoGO = Instantiate(creatureInfoPrefab, this.transform);
        newCreatureInfoGO.GetComponent<CreatureInfoUI>().SetCreature(creature);
    }
}
