﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Класс для первоначальной инициализации 
public class Initializer : MonoBehaviour
{
    //  Все классы-мэнеджеры, которые существуют в единственном экземпляре
    //и требуют инициализации
    public WorldGrid worldGrid;
    public MouseDrawing mouseDrawing;
    public GameController gameController;
    public EnemySpawner enemySpawner;
    public BerrySpawner berrySpawner;
    public PlayerUI ui;

    [Space]
    public float ui_size100scale = 100f;
    
    void Start()
    {
        //Инициализация
        worldGrid.Initialize();
        mouseDrawing.Initialize();
        gameController.Initialize();
        berrySpawner.Initialize();
        enemySpawner.Initialize();
        ui.Initialize();

        //Запуск игры начинается отсюда
        worldGrid.GenerateMap();
    }

    void Update()
    {
        GameSettings.ui_size = ui_size100scale / 100f;
        GameSettings.creatureViewDistance = 3;
    }
}
