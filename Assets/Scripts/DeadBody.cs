﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class DeadBody : IConsumable
    {
        public Dictionary<ParameterType, int> parametersByType = new Dictionary<ParameterType, int>();
        public const int PARAMETERS_LENGTH = 4;
        public DeadBody(Creature originalCreature)
        {
            parametersByType.Add(ParameterType.Endurance, (int)Math.Ceiling((double)originalCreature.Endurance / 4));
            parametersByType.Add(ParameterType.MaxAir,(int)Math.Ceiling((double)originalCreature.MaxAir / 4));
            parametersByType.Add(ParameterType.Strength, (int)Math.Ceiling((double)originalCreature.Strength / 4));
            parametersByType.Add(ParameterType.Protection, (int)Math.Ceiling((double)originalCreature.Protection / 4));
            parametersByType.Add(ParameterType.Reaction, (int)Math.Ceiling((double)originalCreature.Reaction / 4));

        }
        public void Affect(Creature creature)
        {
            creature.Strength += parametersByType[ParameterType.Strength];
            creature.MaxAir += parametersByType[ParameterType.MaxAir];
            creature.Protection += parametersByType[ParameterType.Protection];
            creature.Endurance += parametersByType[ParameterType.Endurance];
            creature.Health += parametersByType[ParameterType.Endurance]*2;
        }
    }
}
