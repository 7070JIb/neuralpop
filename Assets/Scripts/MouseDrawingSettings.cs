﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "DrawingSettings", menuName = "TilemapDrawingSettings")]
public class MouseDrawingSettings : ScriptableObject
{
    public string tilemapName;
    public Tile drawTile;
}
