﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public struct MoveData
    {
        public TileInfo[] tilesData;
        public Vector3Int moveDirection;
        public int stepsCount;
        public List<float> data;
        public static List<string> dataText;
        public static bool IsDataTextWriten = false;
        public const int MOVEDATA_NUM = 920;

        public MoveData(Creature creature, TileInfo[] tilesData_)
        {
            stepsCount = creature.lifeInStepsCount;
            moveDirection = creature.moveDirection;
            tilesData = tilesData_;
            data = new List<float>();
            if (!IsDataTextWriten) dataText = new List<string>();
            data.Add((moveDirection == Vector3Int.up) ? 1 : 0);
            if (!IsDataTextWriten) dataText.Add("move up");
            data.Add((moveDirection == Vector3Int.right) ? 1 : 0);
            if (!IsDataTextWriten) dataText.Add("move right");
            data.Add((moveDirection == Vector3Int.down) ? 1 : 0);
            if (!IsDataTextWriten) dataText.Add("move down");
            data.Add((moveDirection == Vector3Int.left) ? 1 : 0);
            if (!IsDataTextWriten) dataText.Add("move left");
            data.Add((moveDirection == Vector3Int.zero) ? 1 : 0);
            if (!IsDataTextWriten) dataText.Add("move zero");
            data.Add(creature.Health);
            if (!IsDataTextWriten) dataText.Add("creature Health");
            data.Add(creature.Air);
            if (!IsDataTextWriten) dataText.Add("creature Air");
            data.Add(creature.Endurance);
            if (!IsDataTextWriten) dataText.Add("creature Endurance");
            data.Add(creature.MaxAir);
            if (!IsDataTextWriten) dataText.Add("creature MaxAir");
            data.Add(creature.Strength);
            if (!IsDataTextWriten) dataText.Add("creature Strength");
            data.Add(creature.Protection);
            if (!IsDataTextWriten) dataText.Add("creature Protection");
            data.Add(creature.Reaction);
            if (!IsDataTextWriten) dataText.Add("creature Reaction");

            for (int i = 0; i < tilesData.Length; i++)
            {
                for(int j = 0; j < tilesData[i].info.Count; j++)
                {
                    data.Add(tilesData[i].info[j]);
                    if (!IsDataTextWriten) dataText.Add(tilesData[i].infoText[j]);
                }
            }
            IsDataTextWriten = true;
        }
    }
}
