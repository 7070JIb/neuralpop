﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public abstract class CreatureParameterUI: MonoBehaviour
    {
        
        public float value;
        public Creature trackingCreature;
        public void Initialize(Creature creature)
        {
            trackingCreature = creature;
            value = 0;
            InitializeExtra();
        }

        protected virtual void InitializeExtra() { }
        abstract public ParameterType Type { get; }
        public abstract void OnParameterUpdated(float value);
    }
}
