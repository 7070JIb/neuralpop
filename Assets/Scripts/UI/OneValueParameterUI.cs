﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Assets.Scripts.UI
{
    public abstract class OneValueParameterUI : CreatureParameterUI
    {
        [SerializeField]
        protected TMP_Text text;
        [SerializeField]
        protected Image image;
    }
}
