﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public abstract class BarParameterUI : CreatureParameterUI
    {
        [SerializeField] 
        [HideInInspector]
        protected GameObject background;
        [SerializeField]
        [HideInInspector]
        protected Image barL;
        [SerializeField]
        [HideInInspector]
        protected Image barR;
    }
}
