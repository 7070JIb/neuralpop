﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;

namespace Assets.Scripts.UI.PlayerUI
{
    class CreatureAirbar : BarParameterUI
    {

        public override ParameterType Type => ParameterType.Air;

        void Awake()
        {
            barR = this.transform.Find("barR").GetComponent<Image>();
            barL = this.transform.Find("barL").GetComponent<Image>();
        }
        protected override void InitializeExtra()
        {
            trackingCreature.OnParameterChanged_ByType[ParameterType.MaxAir].AddListener(OnMaxAirUpdated);
        }

        private void OnMaxAirUpdated(float value)
        {
            barR.fillAmount = barL.fillAmount = (float)trackingCreature.Air / (float)trackingCreature.MaxAir;
        }

        public override void OnParameterUpdated(float value)
        {
            barR.fillAmount = barL.fillAmount = value;
        }
    }
}
