﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Parameters
{
    class HealthParameterUI : BarParameterUI
    {

        public override ParameterType Type => ParameterType.Health;

        protected override void InitializeExtra()
        {
            trackingCreature.OnParameterChanged_ByType[ParameterType.Endurance].AddListener(OnEnduranceIncreased);
        }

        //value - процент здоровья для хелсбара, от 0 до 1
        public override void OnParameterUpdated(float value)
        {
            barL.fillAmount = value;
            barR.fillAmount = value;
            this.value = value;
        }

        public void OnEnduranceIncreased(float value)
        {
            background.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 30f * GameSettings.ui_size * trackingCreature.Endurance  +6f);
            barR.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 15f * GameSettings.ui_size * trackingCreature.Endurance);
            barL.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 15f * GameSettings.ui_size * trackingCreature.Endurance);
        }
    }
}
