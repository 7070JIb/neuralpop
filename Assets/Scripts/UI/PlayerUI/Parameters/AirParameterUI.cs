﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.UI;
using UnityEngine;

namespace Assets.Scripts.UI.Parameters
{
    class AirParameterUI : BarParameterUI
    {

        public override ParameterType Type => ParameterType.Air;

        protected override void InitializeExtra()
        {
            trackingCreature.OnParameterChanged_ByType[ParameterType.MaxAir].AddListener(OnMaxAirIncreased);
        }
        //value -  от 0 до 1
        public override void OnParameterUpdated(float value)
        {
            barL.fillAmount = value;
            barR.fillAmount = value;
            this.value = value;
        }

        public void OnMaxAirIncreased(float value)
        {
            background.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 30f * GameSettings.ui_size * trackingCreature.MaxAir + 6f);
            barR.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 15f * GameSettings.ui_size * trackingCreature.MaxAir);
            barL.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 15f * GameSettings.ui_size * trackingCreature.MaxAir);
        }
    }
}
