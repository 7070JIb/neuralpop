﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.UI.Parameters
{
    class EnduranceParametersUI : OneValueParameterUI
    {
        public override ParameterType Type => ParameterType.Endurance;

        public override void OnParameterUpdated(float value)
        {
            text.text = value.ToString();
            this.value = value;
        }
    }
}
