﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.UI;
using System;

public class CreatureHealthbar : BarParameterUI 
{

    public override ParameterType Type => ParameterType.Health;

    void Awake()
    {
        barR = this.transform.Find("barR").GetComponent<Image>();
        barL = this.transform.Find("barL").GetComponent<Image>();
    }
    protected override void InitializeExtra()
    {
        trackingCreature.OnParameterChanged_ByType[ParameterType.Endurance].AddListener(OnEnduranceUpdated);
    }

    private void OnEnduranceUpdated(float value)
    {
            barR.fillAmount = barL.fillAmount = (float)trackingCreature.Health / (float)trackingCreature.Endurance;
    }

    public override void OnParameterUpdated(float value)
    {
        barR.fillAmount = barL.fillAmount = value;
    }
}

