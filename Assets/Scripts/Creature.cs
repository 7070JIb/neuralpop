﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;
using Assets.Scripts;

public abstract class Creature : MonoBehaviour
{
    public Dictionary<ParameterType, float> parametersByType = new Dictionary<ParameterType, float>();
    public const int PARAMETERS_LENGTH = 7;
    [SerializeField]
    private float _air;
    public int Air 
    {
        get => (int)_air;
        set 
        {
            if (value < 0) value = 0;
            _air = value;

            if (parametersByType.ContainsKey(ParameterType.Air))
                parametersByType[ParameterType.Air] = value;
            else
                parametersByType.Add(ParameterType.Air, value);

            if (OnParameterChanged_ByType.ContainsKey(ParameterType.Air))
                OnParameterChanged_ByType[ParameterType.Air].Invoke((float)value/(float)MaxAir);
        } 
    }
    [SerializeField]
    private float _maxAir;
    public int MaxAir 
    {
        get => (int)_maxAir;
        set
        {
            _maxAir = value;

            if (parametersByType.ContainsKey(ParameterType.MaxAir))
                parametersByType[ParameterType.MaxAir] = value;
            else
                parametersByType.Add(ParameterType.MaxAir, value);

            if (OnParameterChanged_ByType.ContainsKey(ParameterType.MaxAir))
                OnParameterChanged_ByType[ParameterType.MaxAir].Invoke(value);
        } 
    }
    [SerializeField]
    private float _health;
    public float Health 
    { 
        get => _health; 
        set 
        {
            if (value > Endurance) value = Endurance;
            _health = value;


            if (parametersByType.ContainsKey(ParameterType.Health))
                parametersByType[ParameterType.Health] = value;
            else
                parametersByType.Add(ParameterType.Health, value);

            if (OnParameterChanged_ByType.ContainsKey(ParameterType.Health))
                OnParameterChanged_ByType[ParameterType.Health].Invoke(value/(float)Endurance);
        } 
    }
    [SerializeField]
    private float _endurance;
    public int Endurance 
    {
        get =>(int) _endurance;
        set 
        {
            _endurance = value;

            if (parametersByType.ContainsKey(ParameterType.Endurance))
                parametersByType[ParameterType.Endurance] = value;
            else
                parametersByType.Add(ParameterType.Endurance, value);

            if (OnParameterChanged_ByType.ContainsKey(ParameterType.Endurance))
                OnParameterChanged_ByType[ParameterType.Endurance].Invoke(value);
        } 
    }
    [SerializeField]
    private float _strength;
    public int Strength
    { 
        get => (int)_strength;
        set
        {
            _strength = value;

            if (parametersByType.ContainsKey(ParameterType.Strength))
                parametersByType[ParameterType.Strength] = value;
            else
                parametersByType.Add(ParameterType.Strength, value);

            if (OnParameterChanged_ByType.ContainsKey(ParameterType.Strength))
                OnParameterChanged_ByType[ParameterType.Strength].Invoke(value);
        } 
    }
    [SerializeField]
    private float _protection;
    public int Protection 
    { 
        get =>(int) _protection; 
        set
        {
            _protection = value;

            if (parametersByType.ContainsKey(ParameterType.Protection))
                parametersByType[ParameterType.Protection] = value;
            else
                parametersByType.Add(ParameterType.Protection, value);

            if (OnParameterChanged_ByType.ContainsKey(ParameterType.Protection))
                OnParameterChanged_ByType[ParameterType.Protection].Invoke(value);
        }
    }

    public float maxHunger = 120f;
    public int lifeInStepsCount;

    [SerializeField]
    private float _reaction;
    public int Reaction 
    { 
        get =>(int) _reaction;
        set
        {
            _reaction = value;

            if (parametersByType.ContainsKey(ParameterType.Reaction))
                parametersByType[ParameterType.Reaction] = value;
            else
                parametersByType.Add(ParameterType.Reaction, value);

            if (OnParameterChanged_ByType.ContainsKey(ParameterType.Reaction))
                OnParameterChanged_ByType[ParameterType.Reaction].Invoke(value);
        }
    }

    public bool IsDead = false;
    public bool IsUIConnected = false;
    protected Tilemap creaturesTilemap;
    public Vector3Int creaturePosition;
    public Vector3Int moveDirection = Vector3Int.zero;
    public Color color;
    public Tile creatureTile;

    public UnityEvent OnDamageTaken;
    public UnityEvent OnAirDecreased;
    public UnityEvent OnAirRestored;
    public UnityEvent OnDied;
    public UnityEvent OnReproduced;
    public Dictionary<ParameterType, UnityEvent1<float>> OnParameterChanged_ByType = new Dictionary<ParameterType, UnityEvent1<float>>();


    public Tile emptyTile { get => ((Tile)ScriptableObject.CreateInstance(typeof(Tile))); }
    void Awake()
    {
        OnDamageTaken = new UnityEvent();
        OnDied = new UnityEvent();
        OnReproduced = new UnityEvent();
        creaturesTilemap = WorldGrid.tilemaps[WorldGrid.CREATURES_TILEMAP_NAME];
        SetParameters(1, 1, 1, 1, 1, 1, 1);
        lifeInStepsCount = 0;
        foreach (var parameter in parametersByType)
            OnParameterChanged_ByType.Add(parameter.Key, new UnityEvent1<float>());

        AtAwake();
    }
    void Start()
    {     
        AtStart();
    }
    public abstract void AtStart();
    public abstract void AtAwake();
    public void SetParameters(int endurance, int maxAir,int strength, int protection, int reaction, int health, int air)
    {
        Endurance = endurance;
        MaxAir = maxAir;
        Strength = strength;
        Protection = protection;
        Reaction = reaction;
        Health = health;
        Air = air;
    }
    public abstract void TryToMakeAction();
    protected void MakeStep(Vector3Int direction)
    {
        if (WorldGrid.IsInsideMap(creaturePosition + direction))
        {
            //Перемещение
            {
                creaturesTilemap.SetTile(creaturePosition, emptyTile);
                creaturePosition += direction;
                creaturesTilemap.SetTile(creaturePosition, creatureTile);
                this.transform.position = creaturePosition;
            }
            //Ягоды
            {
                if (BerrySpawner.berries[creaturePosition] != null)
                {
                    BerrySpawner.berries[creaturePosition].Affect(this);
                    BerrySpawner.berries[creaturePosition] = null;
                    BerrySpawner.tilemap.SetTile(creaturePosition, emptyTile);
                }
            }
            //Трупы
            {
                if (WorldGrid.deadBodies[creaturePosition] != null)
                {
                    WorldGrid.deadBodies[creaturePosition].Affect(this);
                    WorldGrid.deadBodies[creaturePosition] = null;
                    WorldGrid.tilemaps[WorldGrid.DEADBODIES_TILEMAP_NAME].SetTile(creaturePosition, emptyTile);
                }
            }
            //Вода
            {
                if (WorldGrid.groundTiles[creaturePosition] == false)
                {
                    //Асфиксия
                    if (Air == 0)
                    {
                        Health -= 1;
                    }

                    Air -= 1;
                }
                else
                {
                    Air = MaxAir;
                }
            }

            //Голод
            {
                if (direction == Vector3Int.zero)
                    Health -= (1 / (1 + Mathf.Exp(-0.015f * (Endurance + MaxAir + Strength + Protection + Reaction - 60))) - 0.3f)/2f;
                    //Health -= 0.2f*(1/maxHunger)*Mathf.Exp((float)(Endurance + MaxAir + Strength + Protection + Reaction)* 0.05f);
                else
                    Health -= (1 / (1 + Mathf.Exp(-0.015f * (Endurance + MaxAir + Strength + Protection + Reaction - 60))) - 0.3f);
                    //Health -= (1 / maxHunger) * Mathf.Exp((float)(Endurance + MaxAir + Strength + Protection + Reaction) * 0.05f);

            }



            //Смерть
            if (Health <= 0) Die();

            //Размножение
            if (!IsDead)
            {
                OnReproduced.Invoke();
            }
        }
    }
    public abstract MoveData CreateMoveData();
    public abstract float[] CreateSituationData();

    public void Die()
    {
        IsDead = true;
        creaturesTilemap.SetTile(creaturePosition, emptyTile);
        OnDied.Invoke();
        WorldGrid.CreateDeadBody(this, creaturePosition);
        WorldGrid.KillCreature(this);
    }

    protected void Attack(Creature opponent)
    {
        opponent.Health -= (Strength + Protection / 4) / (opponent.Protection + opponent.Strength/4);
        Health -= (opponent.Protection + opponent.Strength / 4) / (Strength + Protection / 4);
        opponent.OnDamageTaken.Invoke();
        if (Health <= 0) Die();
        if (opponent.Health <= 0) opponent.Die();
    }

    protected virtual void Reproduce()
    {

    }
}
