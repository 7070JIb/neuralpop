﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace Neural_Network_1
{
    public class NeuralNetwork 
    {
        public double[,,] weights;
        public int[] layers;
        public double[,] outputs;
        public double[,] differences;

        public NeuralNetwork(int[] layers_)
        {
            layers = layers_;
            System.Random rand = new System.Random();

            //Создаются рандомные веса
            weights = new double[layers.Length - 1, layers.Max(), layers.Max()];
            ; for (int i = 0; i < layers.Length - 1; i++)
            {
                for (int j = 0; j < layers[i]; j++)
                {
                    for (int k = 0; k < layers[i + 1]; k++)
                    {
                        weights[i, j, k] = rand.NextDouble() * 2 - 1;
                    }
                }
            }
        }
        public double[] FeedForward(double[] input)
        {
            //Проверка на соответствие размера входящих данных размеру слоя входных данных
            if (input.Length != layers[0]) throw new ArgumentException();

            outputs = new double[layers.Length, layers.Max()];
            //заполнение выходов входного слоя входными данными
            for (int i = 0; i < layers[0]; i++)
            {
                outputs[0, i] = input[i];
            }

            //расчет результата, i - цикл уровня набора слоев
            for (int i = 0; i < layers.Length - 1; i++)
            {
                //k - уровень: слой с нейроном, для которого расчитывается выход
                for (int k = 0; k < layers[i + 1]; k++)
                {
                    //j - уровень: предыдущий слой, у которого суммируются выходы и веса связей со слоем k
                    double sum = 0;
                    for (int j = 0; j < layers[i]; j++)
                    {
                        sum += weights[i, j, k] * outputs[i, j];
                    }
                    //Пропуск через функцию активации
                    outputs[i + 1, k] = Sigm(sum);
                }
            }
            double[] result = new double[layers[layers.Length - 1]];
            for (int i = 0; i < layers[layers.Length - 1]; i++)
            {
                result[i] = outputs[layers.Length - 1, i];
            }
            return result;
        }

        public double Sigm(double x)
        {
            return (1 / (1 + Math.Exp(-x)));
        }

        public void Learn(List<Tuple<List<double>, List<double>>> dataset_input_expected, int epochs, double learningRate)
        {
            for (int i = 0; i < epochs; i++)
            {
                foreach (var dataChunk in dataset_input_expected)
                {
                    Backpropogation(dataChunk.Item1, dataChunk.Item2, learningRate);
                }
            }
        }
        public void Backpropogation(List<double> inputs, List<double> expected, double learningRate)
        {
            differences = new double[layers.Length, layers.Max()];

            //Сначала находим ошибку выходного нейрона
            double[] actual = FeedForward(inputs.ToArray());
            for (int i = 0; i < expected.Count; i++)
            {
                differences[layers.Length - 1, i] = expected[i] - actual[i];
            }

            //Находим ошибки остальных нейронов,начиная с предпоследнего слоя            
            for (int i = layers.Length - 2; i > 0; i--)
            {
                for (int j = 0; j < layers[i]; j++)
                {
                    differences[i, j] = 0;
                    for (int k = 0; k < layers[i + 1]; k++)
                    {
                        differences[i, j] += differences[i + 1, k] * weights[i, j, k];
                    }
                }
            }

            //Корректируем веса
            for (int i = 0; i < layers.Length - 1; i++)
            {
                for (int j = 0; j < layers[i]; j++)
                {
                    for (int k = 0; k < layers[i + 1]; k++)
                    {
                        //вес += выход выход левого нейрона * производная акт функции от выхода правого нейрона *
                        //* коэф обучения * ошибка правого нейрона
                        weights[i, j, k] += outputs[i, j] * (outputs[i + 1, k] * (1 - outputs[i + 1, k])) * learningRate * differences[i + 1, k];
                    }
                }
            }
        }
        public static List<Tuple<List<double>, List<double>>> ParseTVS(string filePath)
        {
            List<Tuple<List<double>, List<double>>> dataset = new List<Tuple<List<double>, List<double>>>();
            using (StreamReader reader = new StreamReader(filePath))
            {
                string[] columnNames = reader.ReadLine().Split('\t');

                int[]  resultColumnsNumbers = new int[5] { 0,1,2,3,4};
                string str = reader.ReadLine();
                while (str != null)
                {
                    string[] dataraw = str.Split('\t');
                    double[] dataparsed = new double[dataraw.Length];
                    for(int i = 0; i < dataraw.Length; i++)
                        dataparsed[i] = double.Parse(dataraw[i]);

                    List<double> featuresList = new List<double>();
                    List<double> result = new List<double>();
                    for (int i = 0; i < dataparsed.Length; i++)
                    {
                        if (resultColumnsNumbers.Contains(i))
                        {
                            result.Add(dataparsed[i]);
                        }
                        else
                        {
                            featuresList.Add(dataparsed[i]);
                        }
                    }

                    dataset.Add(new Tuple<List<double>, List<double>>(featuresList, result));
                    str = reader.ReadLine();
                }
                Debug.Log("По окончанию формуирования в датасете " + dataset.Count + " строк");
                reader.Close();
            }
            return dataset;
        }

        public static async Task<List<Tuple<List<double>, List<double>>>> ParseTVSAsync(string filePath)
        { 
            return await Task.Run(()=>ParseTVS(filePath));
        }
    }
}
