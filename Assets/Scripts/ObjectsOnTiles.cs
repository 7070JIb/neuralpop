﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class ObjectsOnTiles<T>
    {
        private T[,] _objects;
        private int _mapSize;
        public int Length { get => _mapSize; }
        public ObjectsOnTiles(int mapSize)
        {
            _mapSize = mapSize;
            _objects = new T[mapSize, mapSize];  
        }

        public T this[Vector3Int coordinates]
        {

            get 
            {
                try
                {
                    return _objects[coordinates.x + _mapSize / 2, coordinates.y + _mapSize / 2];
                }
                catch(IndexOutOfRangeException e)
                {
                    return default(T);
                }
            }

            set => _objects[coordinates.x + _mapSize / 2, coordinates.y + _mapSize / 2] = value;
        }
        public T this[int x, int y]
        {
            get
            {
                return _objects[x + _mapSize / 2, y + _mapSize / 2];
            }
            set
            {
                _objects[x + _mapSize / 2, y + _mapSize / 2] = value;
                
            }
        }
    }
}
