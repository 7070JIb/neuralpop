﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Berry: IConsumable
{
    public Vector3Int berryPosition;
    public abstract ParameterType Type { get;}
    public Berry(Vector3Int position)
    {
        berryPosition = position;
    }



    public abstract void Affect(Creature creature); 

}

public enum ParameterType
{
    Endurance,
    Strength,
    Protection,
    MaxAir,
    Reaction,
    Air,
    Health
}