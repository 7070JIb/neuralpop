﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    class StrengthBerry : Berry
    {
        public StrengthBerry(Vector3Int position) : base(position) { }
        public override ParameterType Type { get => ParameterType.Strength; }

        public override void Affect(Creature creature)
        {
            creature.Health++;
            creature.Strength++;
        }
    }
}
