﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{


    class EnduranceBerry : Berry
    {
        public EnduranceBerry(Vector3Int position) : base(position) { }

        public override ParameterType Type { get => ParameterType.Endurance; }
        public override void Affect(Creature creature)
        {
            creature.Endurance++;
            creature.Health++;
        }
    }
}
