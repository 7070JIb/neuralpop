﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    class ProtectionBerry : Berry
    {
        public ProtectionBerry(Vector3Int position) : base(position) { }
        public override ParameterType Type { get => ParameterType.Protection; }

        public override void Affect(Creature creature)
        {
            creature.Health++;
            creature.Protection++;
        }
    }
}
