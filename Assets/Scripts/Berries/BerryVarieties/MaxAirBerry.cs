﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    class MaxAirBerry : Berry
    {
        public MaxAirBerry(Vector3Int position) : base(position) { }
        public override ParameterType Type { get => ParameterType.MaxAir; }
        public override void Affect(Creature creature)
        {
            creature.Health += 1;
            creature.MaxAir += 1;
        }
    }
}
