﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    class ReactionBerry : Berry
    {
        public ReactionBerry(Vector3Int position) : base(position) { }
        public override ParameterType Type { get => ParameterType.Reaction; }

        public override void Affect(Creature creature)
        {
            creature.Health++;
            creature.Reaction++;
        }
    }
}
