﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public struct TileInfo
    {
        public List<float> info;
        public List<string> infoText;
        public TileInfo(List<float> info_, List<string> infoText_)
        {
            info = info_;
            infoText = infoText_;
        }
        public static TileInfo ReadFromTile(Vector3Int position, Creature relationCreature)
        {
            List<float> info = new List<float>();
            List<string> infoText = new List<string>();
            string coords = $"({(position - relationCreature.creaturePosition).x},{(position - relationCreature.creaturePosition).y})";

                //Есть ли земля
                info.Add((WorldGrid.IsInsideMap(position) && (WorldGrid.groundTiles[position] == true)) ? 1 : 0);
                if (!MoveData.IsDataTextWriten) infoText.Add(coords + " суша");
                //Присутствуют ли разные типы ягод
                for (int i = 0; i < 5; i++)
                {
                    if (BerrySpawner.berries[position] != null)
                    {
                        info.Add((WorldGrid.IsInsideMap(position)&&((int)BerrySpawner.berries[position].Type == i)) ? 1 : 0);
                        if (!MoveData.IsDataTextWriten) infoText.Add("ягода " + BerrySpawner.berries[position].Type);
                    }
                    else
                    {
                        info.Add(0);
                        if (!MoveData.IsDataTextWriten) infoText.Add("ягод нет");
                    }
                }
                //Присутствует ли существо
                info.Add(WorldGrid.GetCreature(position, out _) ? 1 : 0);
                if (!MoveData.IsDataTextWriten) infoText.Add("существо");
                //Параметры существа (если нет - 0);
                for (int i = 0; i < Creature.PARAMETERS_LENGTH; i++)
                {
                    info.Add(WorldGrid.GetCreature(position, out Creature __creature) ? __creature.parametersByType[(ParameterType)i] : 0);
                    if (!MoveData.IsDataTextWriten) infoText.Add(((ParameterType)i).ToString());
                }
                //Присутствует ли труп
                info.Add((WorldGrid.deadBodies[position] != null) ? 1 : 0);
                if (!MoveData.IsDataTextWriten) infoText.Add("труп");
                //Пареметры трупа (если нет - 0)
                for (int i = 0; i < DeadBody.PARAMETERS_LENGTH; i++)
                {
                    info.Add((WorldGrid.deadBodies[position] != null) ? WorldGrid.deadBodies[position].parametersByType[(ParameterType)i] : 0);
                    if (!MoveData.IsDataTextWriten) infoText.Add(((ParameterType)i).ToString());
                }
            
            return new TileInfo(info, infoText);
        }
    }
}
