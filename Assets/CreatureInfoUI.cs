﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;
using Assets.Scripts.UI;

public class CreatureInfoUI : MonoBehaviour
{
    Action updateAction;

    public Creature creature;

    private RectTransform _transform;


    public void SetCreature(Creature creature)
    {
        this.creature = creature;
        updateAction = StickToCreature;
        _transform = this.GetComponent<RectTransform>();

        foreach (var parameter in this.transform.GetComponentsInChildren<CreatureParameterUI>())
        {
            creature.OnParameterChanged_ByType[parameter.Type].AddListener(parameter.OnParameterUpdated);
            parameter.Initialize(creature);
        }
        //Эти циклы должны быть раздельными
        foreach (var parameter in this.transform.GetComponentsInChildren<CreatureParameterUI>())
        {
            creature.OnParameterChanged_ByType[parameter.Type].Invoke(creature.parametersByType[parameter.Type]);
        }

        creature.OnDied.AddListener(()=>GameObject.Destroy(this.gameObject));
    }

    void StickToCreature()
    {
        _transform.position = Camera.main.WorldToScreenPoint(creature.creaturePosition + new Vector3(0.5f, 0.5f, 0));
        _transform.localScale = new Vector3(10/MainCamera.Size, 10 / MainCamera.Size);
    }
    void DoNothing() { }
    void LateUpdate()
    {
        updateAction();
    }
}
