﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class WorldGrid : MonoBehaviour
{
    public const string GROUND_TILEMAP_NAME = "Ground";
    public const string BERRIES_TILEMAP_NAME = "Berries";
    public const string DEADBODIES_TILEMAP_NAME = "DeadBodies";
    public const string CREATURES_TILEMAP_NAME = "Creatures";
    public const string ARROWS_TILEMAP_NEM = "Arrows";

    //Синглтон
    public static WorldGrid Instance { get; private set; }

    //Все тайлмапы, находятся автоматически на старте
    public static Dictionary<string, Tilemap> tilemaps;

    public Tile deadBodyTile;
    
    //Все существа на карте, изменяется при спавне или смерти существа
    public List<Creature> allCreatures;
    public static ObjectsOnTiles<bool> groundTiles;
    public static ObjectsOnTiles<Creature> creaturesOnTiles;
    public static ObjectsOnTiles<DeadBody> deadBodies;
    //Образец тайла земли
    public Tile groundTile;

    //Событие генерации карты
    public static UnityEvent OnMapGenerated;

    [Space]
    //Параметры генерации карты
    public int mapSize;
    public float noiseScale;
    public float complexity;
    public float waterLevel;
    public int power;

    public static int MapSize { get => Instance.mapSize; }
    //Начальная инициализация
    public void Initialize()
    {
        Instance = this;
        if (mapSize % 2 == 1)
        {
            mapSize++;
        }

        tilemaps = new Dictionary<string, Tilemap>();
        Tilemap [] _tilemaps = this.transform.GetComponentsInChildren<Tilemap>();
        foreach(var tilemap in _tilemaps)
        {
            tilemaps.Add(tilemap.name, tilemap);
        }

        OnMapGenerated = new UnityEvent();
        OnMapGenerated.AddListener(CreateGroundMap);
        groundTiles = new ObjectsOnTiles<bool>(mapSize);
        creaturesOnTiles = new ObjectsOnTiles<Creature>(mapSize);
        deadBodies = new ObjectsOnTiles<DeadBody>(mapSize);
    }

    //Генерация карты
    public void GenerateMap()
    {
        OnMapGenerated.Invoke();
    }

    //Метод для поулчения ссылки на существо в нужной клетке
    public static bool GetCreature(Vector3Int position, out Creature foundCreature)
    {
        foundCreature = null;
        foreach (var creature in Instance.allCreatures)
        {
            if (creature.creaturePosition == position && !creature.IsDead)
            {
                foundCreature = creature;
                return true;
            }
        }
        return false;
    }

    internal static void CreateDeadBody(Creature creature, Vector3Int creaturePosition)
    {
        deadBodies[creaturePosition] = new DeadBody(creature);
        Tile deadBody = (Tile)ScriptableObject.CreateInstance(typeof(Tile));
        deadBody.sprite = Instance.deadBodyTile.sprite;
        deadBody.color = creature.color;
        tilemaps[DEADBODIES_TILEMAP_NAME].SetTile(creaturePosition, deadBody); 
    }

    //Проверка тайла на наличие земли в области 3x3 с центром в position
    public bool CanSpawnHere(Vector3Int position)
    {
        for (int x = position.x - 1; x <= position.x + 1; x++)
        {
            for (int y = position.y - 1; y <= position.y + 1; y++)
            {
                if ( tilemaps[WorldGrid.GROUND_TILEMAP_NAME].GetTile(new Vector3Int(x, y, 0)) != groundTile||!IsInsideMap(new Vector3Int(x, y, 0)))
                {
                    return false;
                }
            }
        }
        return true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            OnMapGenerated.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            KillAllEnemies();
        }
    }
    public void KillAllEnemies()
    {
        KillRecursive(allCreatures.Count-1);
    }

    void KillRecursive(int n)
    {
        if (n >=0 )
        {
            Debug.Log("n=" + n);
            if (allCreatures[n] != GameController.player)
            {
                allCreatures[n].Die();
            }
            else
            {
                n--;
            }
            KillRecursive(--n);
        }
    }

    public static void KillCreature(Creature creature)
    {
        Destroy(creature.gameObject);
    }
    public float[,] RecursiveLayersX2step(float scale, int mapSize, int power, float complexity)
    {
        int offsetX = UnityEngine.Random.Range(-1024, 1024);
        int offsetY = UnityEngine.Random.Range(-1024, 1024);

        float[,] perlinMap = new float[mapSize, mapSize];
        for (int x = 0; x < mapSize; x++)
            for (int y = 0; y < mapSize; y++)
                perlinMap[x, y] = 1;

        for (int i = 0; i < power; i++)
        {
            for (int x = 0; x < mapSize; x++)
            {
                for (int y = 0; y < mapSize; y++)
                {
                    perlinMap[x, y] = perlinMap[x, y] * (Mathf.PerlinNoise((x + mapSize / 2 + offsetX) * scale, (y + mapSize / 2 + offsetY) * scale)) * 2;
                }
            }
            scale *= complexity;
        }

        return perlinMap;
    }

    public void CreateGroundMap()
    {
        tilemaps[WorldGrid.GROUND_TILEMAP_NAME].ClearAllTiles();

        float[,] perlinMap = RecursiveLayersX2step(noiseScale,mapSize,power,complexity);
        for (int x = 0; x < mapSize; x++)
        {
            for (int y = 0; y < mapSize; y++)
            {
                if (perlinMap[x, y] > waterLevel)
                {
                    tilemaps[WorldGrid.GROUND_TILEMAP_NAME].SetTile(new Vector3Int(x - mapSize / 2, y - mapSize / 2, 0), groundTile);
                    groundTiles[new Vector3Int(x - mapSize / 2, y - mapSize / 2, 0)] = true;
                }
                else
                {
                    groundTiles[new Vector3Int(x - mapSize / 2, y - mapSize / 2, 0)] = false;
                }
            }
        }
    }

    public static bool IsInsideMap(Vector3Int position)
    {
        if (position.x >= -Instance.mapSize / 2 && position.x < Instance.mapSize / 2 && position.y >= -Instance.mapSize / 2 && position.y < Instance.mapSize / 2)
            return true;
        else return false;
    }
}
