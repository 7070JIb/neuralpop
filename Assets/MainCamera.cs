﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    public Player player;

    private static Camera mainCamera;
    public static float Size { get => mainCamera.orthographicSize; }

    void Update()
    {
        if(Input.GetKey(KeyCode.Equals))
        {
            mainCamera.orthographicSize -= Time.deltaTime*5;
        }
        if (Input.GetKey(KeyCode.Minus))
        {
            mainCamera.orthographicSize += Time.deltaTime*5;
        }
    }
    void Start()
    {
        mainCamera = this.GetComponent<Camera>();
    }
    // Update is called once per frame
    void LateUpdate()
    {
         if(player != null) this.transform.position = player.creaturePosition + new Vector3(0.5f,0.5f,-1);
    }
}
