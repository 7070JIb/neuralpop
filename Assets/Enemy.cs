﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Assets.Scripts;

public class Enemy : Creature
{
    public Tile enemyTile;

    public override void TryToMakeAction()
    {
        moveDirection = new Vector3Int[] { Vector3Int.up, Vector3Int.right, Vector3Int.down, Vector3Int.left }[Random.Range(0, 4)];

        if (WorldGrid.GetCreature(creaturePosition + moveDirection, out Creature opponent) && opponent != this)
        {
            Attack(opponent);
        }
        else
        {
            MakeStep(moveDirection);
        }
    }

    

    public override void AtStart()
    {
        Endurance = 1;
        Health = 1;
        Strength = 1;
        Protection = 1;
        Reaction = 1;
        MaxAir = 1;
        Air = 1;
        GameController.OnCreatureSpawned.Invoke(this);
        IsUIConnected = true;
        creatureTile = (Tile)ScriptableObject.CreateInstance(typeof(Tile));
        creatureTile.sprite = enemyTile.sprite;
        creatureTile.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        color = creatureTile.color;


        
    }

    public override void AtAwake()
    {
        WorldGrid.Instance.allCreatures.Add(this);
    }

    public override MoveData CreateMoveData()
    {
        return default(MoveData);
    }

    public override float[] CreateSituationData()
    {
        return null;
    }
}
