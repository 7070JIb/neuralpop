﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Reflection;

public class BerrySpawner : MonoBehaviour
{
    public int spawnTime;
    [Space]
    public float scale;
    public float complexity;
    public int power;
    public float waterLevel;
    public Tile[] berryTiles;
    public static Tilemap tilemap;

    Dictionary<ParameterType, Type> berriesByTypes = new Dictionary<ParameterType, Type>();

    public static ObjectsOnTiles<Berry> berries;

    void Start()
    {
    }
    public void Initialize()
    {
        tilemap = WorldGrid.tilemaps[WorldGrid.BERRIES_TILEMAP_NAME];

        GameController.OnStepMade.AddListener(SpawnBerriesOnce);
        WorldGrid.OnMapGenerated.AddListener(OnGameLoaded);

        berriesByTypes.Add(ParameterType.Endurance, typeof(EnduranceBerry));
        berriesByTypes.Add(ParameterType.Strength, typeof(StrengthBerry));
        berriesByTypes.Add(ParameterType.Protection, typeof(ProtectionBerry));
        berriesByTypes.Add(ParameterType.MaxAir, typeof(MaxAirBerry));
        berriesByTypes.Add(ParameterType.Reaction, typeof(ReactionBerry));
    }
    void OnGameLoaded()
    {
        berries = new ObjectsOnTiles<Berry>(WorldGrid.Instance.mapSize);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            SpawnBerriesOnce();
        }
    }
    public void StopSpawning()
    {
    }

    void SpawnBerriesOnce()
    {
        Debug.Log("BerriesSpawned");
        for (int type = 0; type < berriesByTypes.Count; type++)
        {
            float[,] noiseMap = WorldGrid.Instance.RecursiveLayersX2step(scale, WorldGrid.Instance.mapSize, power, complexity);

            for (int x = 0; x < noiseMap.GetLength(0); x++)
                for (int y = 0; y < noiseMap.GetLength(0); y++)
                {
                    if (noiseMap[x, y] > waterLevel && tilemap.GetTile(new Vector3Int(x, y, 0)) == null)
                    {
                        Vector3Int coords = new Vector3Int(x - noiseMap.GetLength(0) / 2, y - noiseMap.GetLength(0) / 2, 0);
                        if (WorldGrid.tilemaps[WorldGrid.GROUND_TILEMAP_NAME].GetTile(coords) != null)
                        {
                            Type t = berriesByTypes[(ParameterType)type];
                            berries[coords] = (Berry)t.GetConstructors()[0].Invoke(new object[] { coords });
                            tilemap.SetTile(coords, berryTiles[type]);
                        }
                    }
                }
        }
    }
}
