﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.UI;
using UnityEngine.UI;

public class NNSuggestionUI : CreatureParameterUI
{
    Player player;

    public Sprite[] arrows;
    public Image arrowImage;
    protected override void InitializeExtra()
    {
        player = (Player)trackingCreature;
        player.OnSuggestionMade.AddListener(OnParameterUpdated);
    }
    public override ParameterType Type => throw new System.NotImplementedException();

    public override void OnParameterUpdated(float value)
    {
        arrowImage.sprite = arrows[(int)value];
        Debug.Log("Должно было обновиться со значением: " + value);
    }
}
